import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Bootstrap } from "react-bootstrap";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Welcome to My React App!</p>
        <a
          className="App-link"
          href="https://github.com/JavaTheHutt"
          target="_blank"
          rel="noopener noreferrer"
        >
          Visit my Github
        </a>
      </header>
    </div>
  );
}

export default App;
